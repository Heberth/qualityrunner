﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour {

	public GameObject[] obstacles;
	public Vector2 numberOfObstacles;
	public GameObject coin;
	public Vector2 numberOfCoins;

	public List<GameObject> newObstacles;
	public List<GameObject> newCoins;

	// Use this for initialization
	void Start () {

		int newNumberOfObstacles = (int)Random.Range(numberOfObstacles.x, numberOfObstacles.y);
		int newNumberOfCoins = (int)Random.Range(numberOfCoins.x, numberOfCoins.y);

		for (int i = 0; i < newNumberOfObstacles; i++)
		{
			newObstacles.Add(Instantiate(obstacles[Random.Range(0, obstacles.Length)], transform));
			newObstacles[i].SetActive(false);
		}

		for (int i = 0; i < newNumberOfCoins; i++)
		{
			newCoins.Add(Instantiate(coin, transform));
			newCoins[i].SetActive(false);
		}

		PositionateObstacles();
		PositionateCoins();

	}
	
	void PositionateObstacles()
	{
		for (int i = 0; i < newObstacles.Count; i++)
		{
			float posZMin = (297f / newObstacles.Count) + (297f / newObstacles.Count) * i;
			float posZMax = (297f / newObstacles.Count) + (297f / newObstacles.Count) * i + 1;
			newObstacles[i].transform.localPosition = new Vector3(0, 0, Random.Range(posZMin, posZMax));
			newObstacles[i].SetActive(true);
			if (newObstacles[i].GetComponent<ChangeLane>() != null)
				newObstacles[i].GetComponent<ChangeLane>().PositionLane();
		}
	}

	void PositionateCoins()
	{
		float minZPos = 5f;
		bool changedLane = false;
		int laneCoins = 0;
		int laneCoinsCount = 0;
		int ancestorLane = 0;
		for (int i = 0; i < newCoins.Count; i++)
		{
			float maxZPos = minZPos + 10f;
			float randomZPos = Random.Range(minZPos, maxZPos);
				newCoins[i].transform.localPosition = new Vector3(ancestorLane, transform.position.y, randomZPos);
			if (changedLane)
			{
				laneCoinsCount++;
				if (laneCoinsCount > laneCoins)
				{
					changedLane = false;
					laneCoinsCount = 0;
				}
			}
			newCoins[i].SetActive(true);
			if (!changedLane) {
				laneCoins = Random.Range(3,5);
				newCoins[i].GetComponent<ChangeLane>().CoinPositionLane();
				ancestorLane = (int)newCoins[i].transform.position.x;
				changedLane = true;
			}
			minZPos = randomZPos + 1;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			transform.position = new Vector3(0, 0, transform.position.z + 315 * 2);
			PositionateObstacles();
			PositionateCoins();
		}
	}


}