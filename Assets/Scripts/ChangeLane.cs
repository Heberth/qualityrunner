﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLane : MonoBehaviour {

	public void PositionLane()
	{
		int randomLane = Random.Range(-1, 2);
		transform.position = new Vector3(randomLane, transform.position.y, transform.position.z);
	}

	public void CoinPositionLane() {
		int currentLane = (int)transform.position.x + 2;
		int random = Random.Range(0,2);
		if (random == 1)
		{
			currentLane++;	
		}else
		{
			currentLane--;
		}
		if (currentLane > 0 && currentLane < 4)
		{		
			transform.position = new Vector3(currentLane - 2 , transform.position.y, transform.position.z + 3);
		} 
	}
}